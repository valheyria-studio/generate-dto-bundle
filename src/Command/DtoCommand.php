<?php

declare(strict_types=1);

namespace ValheyriaStudio\DesignPatternBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class DtoCommand extends Command
{
    protected static $defaultName = 'design-pattern:dto';
    private static string $description = 'Generate DTO from interactive command or from specific class file.';

    protected function configure(): void
    {
        $this
            ->setName(DtoCommand::$defaultName)
            ->setDescription(DtoCommand::$description)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->success('[DesignPattern] DTO correctly generated !');

        return 1;
    }
}
