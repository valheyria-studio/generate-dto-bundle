<?php

declare(strict_types=1);

namespace ValheyriaStudio\DesignPatternBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class DesignPatternBundle extends Bundle
{
    
}