# Design Pattern Bundle

- [Design Pattern Bundle](#design-pattern-bundle)
- [Installation](#installation)
- [Configuration](#configuration)
- [Usage](#usage)
- [Contribution](#contribution)

# Installation

```console
composer require valheyria-studio/design-pattern-bundle
```

# Configuration

The bundle comes with a sensible default configuration, which is listed below.
You can define these options if you need to change them:

```yaml
# config/packages/design_pattern.yaml
design_pattern:
    components:
      path: 'src/Component'
      # TODO: Remove namespace : Can be determined from the path
      namespace: 'App\Component'
```

# Usage

Console command are defined in namespace : `desing-pattern` and can be run with : `bin/console design-pattern:<command>`

## Command List

| Command | Description                                                         |
|---------|---------------------------------------------------------------------|
| dto     | Generate DTO class from input you send or based from external class |


# Contribution

WIP
